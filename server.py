import tornado.ioloop
import tornado.web
import tornado.websocket
from functools import wraps
from jose import jwt
import json
from urllib.request import urlopen
import os

allowed_origins = ["http://localhost:3000"]

AUTH0_DOMAIN = os.environ["AUTH_DOMAIN"]
API_AUDIENCE = os.environ["AUTH_AUDIENCE"]
ALGORITHMS = os.environ["AUTH_ALGO"]


def get_access_token_from_query_param(args):
    """"Obtain the Access Token from the """
    access_token = None
    if "access_token" not in args.request.arguments:
        raise AuthError(
            {
                "code": "access_token_missing",
                "description": "access_token query param expected",
            },
            1002,
        )

    access_token = args.get_query_argument("access_token")
    if access_token is None:
        raise AuthError(
            {
                "code": "access_token_not_present",
                "description": "No ACCESS TOKEN in query param",
            },
            1002,
        )

    return access_token


def get_jwt_token_from_query_param(args):
    """"Obtain the Access Token from the """
    token = None
    if "ws_token" not in args.request.arguments:
        raise AuthError(
            {
                "code": "ws_token_missing",
                "description": "ws_token query param expected",
            },
            1002,
        )

    token = args.get_query_argument("ws_token")
    if token is None:
        raise AuthError(
            {
                "code": "ws_token_not_present",
                "description": "No JWT in query param",
            },
            1002,
        )

    return token


def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = get_jwt_token_from_query_param(args[0])
        access_token = get_access_token_from_query_param(args[0])
        jsonurl = urlopen("https://" + AUTH0_DOMAIN + "/.well-known/jwks.json")
        jwks = json.loads(jsonurl.read())
        unverified_header = jwt.get_unverified_header(token)
        rsa_key = {}

        for key in jwks["keys"]:
            if key["kid"] == unverified_header["kid"]:
                rsa_key = {
                    "kty": key["kty"],
                    "kid": key["kid"],
                    "use": key["use"],
                    "n": key["n"],
                    "e": key["e"],
                }

        if rsa_key:
            try:
                payload = jwt.decode(
                    token,
                    rsa_key,
                    algorithms=ALGORITHMS,
                    audience=API_AUDIENCE,
                    issuer="https://" + AUTH0_DOMAIN + "/",
                    access_token=access_token,
                )
            except jwt.ExpiredSignatureError:
                raise AuthError(
                    {
                        "code": "token_expired",
                        "description": "token has expired",
                    },
                    1002,
                )

            except jwt.JWTClaimsError:
                raise AuthError(
                    {
                        "code": "invalid_claims",
                        "description": "incorrect claim",
                    },
                    1002,
                )

            except Exception:
                raise AuthError(
                    {
                        "code": "invalid_header",
                        "description": "unable to parse authentication",
                    },
                    1002,
                )

        return f(*args, **kwargs)

    return decorated


def same_origin(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        origin = None
        if "Origin" in args[0].request.headers:
            origin = args[0].request.headers.get("Origin")
        if origin is None:
            args[0].close(code=1002, reason="no origin found")
        if not args[0].check_origin(origin):
            args[0].close(code=1002, reason="invalid origin")
        return f(*args, **kwargs)

    return decorated


class AuthError(Exception):
    def __init__(self, error, status_code):
        self.error = error
        self.status_code = status_code


class EchoWebSocket(tornado.websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    @same_origin
    @requires_auth
    def open(self):
        print("WebSocket opened")

    @same_origin
    def on_message(self, message):
        self.write_message(u"You said: " + message)

    def on_close(self):
        print("WebSocket closed")


def main():
    return tornado.web.Application([(r"/", EchoWebSocket)])


if __name__ == "__main__":
    app = main()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
